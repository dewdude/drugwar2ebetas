Prior to 1.04 of Drugwar//e, I really didn't use any git or anything 
because I'm not enough of a programmer to know WTF to really do with 
them. My way of doing it was just saving to a unique file each time I 
was satisfied with a release. This isn't really that big of a 
project...though someone said "700 lines ain't no joke." So under 
peer-pressure I decided to throw it up on GitLAB...as they all 
assured me git would be useful. 


I decided to make the initial commits using the most recent release 
versions. Most of this code is pretty "mature", and largely all that 
will occur before the expansion is written is just optimizing the BASIC 
more; combining as many things I can on one like where necessary. Each 
basic line has a couple of bytes over overhead; so if you can reduce the 
number of lines by a good number; it adds up.

Just to get a feel of what git will do with a project this small; I 
decided to start commiting to it from the very first alpha; all the way 
to the 1.03 releases. Whether or not I'll add later versions depends on 
how this goes and what it does. At the very least, it will allow any 
curious party to take a look at the progression of the game.
